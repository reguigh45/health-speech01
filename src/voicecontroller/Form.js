import React, { Fragment, useState  } from 'react'
import './style.css'
import logo from './img/logo.jpg';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';





export default function Form() {
  const [list,setList]=useState([]);
  const [transcript,setTranscript]=useState("")
  const [medicament,setMedicament]=useState("")
  
  
 
  

  
  
/**
  you can use whisperai()
 */
function record(){
    var speech = true;
    const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    const recognition = new SpeechRecognition();
    recognition.lang = 'fr-FR';
    recognition.interimResults = true;

    recognition.addEventListener('result', e => {
        const transcript = Array.from(e.results)
        
            .map(result => result[0])
            .map(result => result.transcript)
            .join('')
            console.log( "_______",e.results.SpeechRecognitionResultList)

        document.getElementById("convert_text").value = transcript;
        setTranscript(transcript);
        setMedicament(transcript)
        console.log(transcript);
    });
    
    if (speech === true) {
        recognition.start();
    }
}

  function add(){
  console.log(list,medicament)
  if (!list.find(m=>m===medicament)&& medicament!==""){
setList((list)=> [...list,transcript])


  }
  else{
    alert("medicament deja ajouté ou tu dois inseré le medicament")
  }
    

 }
function vider(){
  setList((list)=>[])
  setMedicament("")
}
const deleteMedicament = (indexToRemove) => {
  const newList = list.filter((item, index) => index !== indexToRemove);
  setList(newList);
  console.log(list,medicament);

}
function print(){
  window.print()
}
  return (
    <Fragment >
      

      <div id='contenu'  >
    
    <div className="voice_to_text">

		<h1> HealthSpeech</h1>
    
    <input  id="convert_text" value={medicament}  onChange={(e)=>{setMedicament(e.target.value) 
      setTranscript(e.target.value)}}></input>
    
    <button className="button-19" onClick={record}  >Speak</button>
    <button className="button-19" onClick={print}>PRINT</button>
    <button className="button-19" onClick={vider}>NEW </button>
    <button className="button-19" onClick={add}>ADD</button>
    <Link to='/enr'><button className="button-19" >SAVE</button></Link>
  
    <Link to='/find'><button className="button-19" id='btn'>find</button></Link>
    
  </div>
  <br/>
  <div id='print'>
    <div className="container">
    <img src={logo} alt='' className="image"  />
    <p>Ordonnance </p>
    </div>
    
    <ul>
      {
      list.map((ls,index)=>{
        return(
          <li key={index} id='txt'>-  {ls}  <button className="button-24" onClick={() => deleteMedicament(index)} id='btn'>x</button> </li>
        )
      })}
    </ul>
    
    
  </div>
  
  </div>
         
  </Fragment>
  )
}


