
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Form from './voicecontroller/Form';


import Enregister from './reduxenre/Enregistrer';
import ListPatient from './reduxenre/listPatient';
import Find from './reduxenre/Find';

function App() {
  
  return (
    <>
    
    <BrowserRouter>
    
    <Routes>
      <Route path='' element={<Form/>}></Route>
      <Route path='/enr' element={<Enregister/>}></Route>
      <Route path='/list' element={<ListPatient/>}></Route>
      <Route path='/find' element={<Find/>}></Route>
      
    </Routes>
    </BrowserRouter>
    
    </>
  );
}

export default App;
