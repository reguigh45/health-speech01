import { applyMiddleware, legacy_createStore as createStore } from "redux";

import { ordonnaceReducer } from "./reducerordannace";

import { thunk } from "redux-thunk";
const store1 = createStore(ordonnaceReducer,applyMiddleware(thunk))
export default store1;
