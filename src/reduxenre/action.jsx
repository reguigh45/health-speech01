import * as type from './typeAction'
import axios from 'axios';
export function postPatient(patient) {
    return function(dispatch, getState) {
      
      
  
      let p = axios.post('http://localhost:3500/patient', patient);
      p.then((response) => {
        dispatch({type: type.POST_PATIENT_SUCCESS,payload: response.data})})
      .catch((error) => {dispatch({type: "POST_PATIENT_FAILURE",payload: error.message })});
      return p;
    };
  }
export function postOrdannace(ordannece) {
    return function(dispatch, getState) {
      
      
  
      let p = axios.post('http://localhost:3500/ordonnances',ordannece );
      p.then((response) => {
        dispatch({type: type.POST_ORDONNACE_SUCCESS,payload: response.data})})
      .catch((error) => {dispatch({type: "POST_ORDONNACE_FAILURE",payload: error.message })});
      return p;
    };
  }