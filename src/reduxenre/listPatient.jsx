import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function ListPatient() {
    const [patients, setPatients] = useState([]);
    const [prescriptions, setPrescriptions] = useState([]);
    const [searchCIN, setSearchCIN] = useState('');
    const [filteredPatient, setFilteredPatient] = useState(null);

    useEffect(() => {
        fetch('http://localhost:3500/patient')
            .then(res => res.json())
            .then(data => setPatients(data));
    }, []);

    useEffect(() => {
        fetch('http://localhost:3500/ordonnances')
            .then(res => res.json())
            .then(data => setPrescriptions(data));
    }, []);

    // Function to filter patients by CIN
    const searchPatientByCIN = () => {
        const foundPatient = patients.find(patient => patient.cin === searchCIN);
        setFilteredPatient(foundPatient);
    };

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    {/* Input field to search by CIN */}
                    <input 
                        type="text" 
                        className="form-control mb-2" 
                        placeholder="Search by CIN" 
                        value={searchCIN} 
                        onChange={(e) => setSearchCIN(e.target.value)} 
                    />
                </div>
                <div className="col-md-6">
                    <button className="btn btn-primary mb-2" onClick={searchPatientByCIN}>Search</button>
                </div>
            </div>

           
            {filteredPatient && (
                <div className="card">
                    <div className="card-body">
                        <h2 className="card-title">Patient Information:</h2>
                        <p className="card-text">
                            ID: {filteredPatient.id} <br />
                            Nom: {filteredPatient.nom} <br />
                            Prenom: {filteredPatient.prenom} <br />
                            CIN: {filteredPatient.cin} <br />
                            Sexe: {filteredPatient.sex} <br />
                            Date: {filteredPatient.date}
                        </p>
                    </div>
                </div>
            )}

            {/* Display prescriptions */}
            <div className="card mt-3">
                <div className="card-body">
                    <h2 className="card-title">Prescriptions:</h2>
                    <ul className="list-group">
                        {prescriptions.map((prescription, index) => (
                            <li key={index} className="list-group-item">
                                ID: {prescription.id}, Medicaments: {prescription.medicament.join(', ')}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    );
}
