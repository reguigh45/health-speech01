import { applyMiddleware, legacy_createStore as createStore } from "redux";

import { patientReducer } from "./reducerpatient";

import { thunk } from "redux-thunk";
const store1 = createStore(patientReducer,applyMiddleware(thunk))
export default store1;
