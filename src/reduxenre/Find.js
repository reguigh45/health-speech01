import React, { useState, useEffect } from 'react';

function Find() {
    const [patient, setPatient] = useState(null);
    const [prescriptions, setPrescriptions] = useState([]);
    const [id, setId] = useState('');

    useEffect(() => {
        // Function to search for a patient by CIN
        const searchPatientByCIN = async () => {
            try {
                const response = await fetch('http://localhost:3500/patient');
                const data = await response.json();
                // Find the patient by CIN
                const foundPatient = data.find(patient => patient.cin === id);
                if (foundPatient) {
                    setPatient(foundPatient);
                    // Search for prescriptions of the found patient
                    const prescriptionsResponse = await fetch('http://localhost:3500/ordonnances');
                    const prescriptionsData = await prescriptionsResponse.json();
                    // Filter prescriptions based on patient ID
                    const patientPrescriptions = prescriptionsData.filter(prescription => prescription.id === foundPatient.id);
                    setPrescriptions(patientPrescriptions);
                } else {
                    setPatient(null); // Patient not found
                    setPrescriptions([]); // Reset prescriptions
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        // Call the search function
        if (id) { // Only call if id is not empty
            searchPatientByCIN();
        }
    }, [id]); // Dependency on id

    return (
        <div>
            <input type='text' placeholder="CIN" value={id} onChange={(e) => setId(e.target.value)} />

            {patient && (
                <div>
                    <h2>Patient Information:</h2>
                    <p>ID: {patient.id} <br/>
                    Nom: {patient.nom}<br/>
                    Prenom: {patient.prenom}<br/>
                    CIN: {patient.cin}<br/>
                    Sexe: {patient.sex}<br/>
                    Date: {patient.date}</p>
                </div>
            )}
            <h2>Dernière Ordonnance:</h2>
            <ul>
                {prescriptions.map((prescription, index) => (
                    <li key={index}>
                         Medicaments: {prescription.medicament.join(', ')}
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default Find;
