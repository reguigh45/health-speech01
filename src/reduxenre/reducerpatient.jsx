import * as type from './typeAction';
export function patientReducer(state = { patient: [],ordonnace: [], loading: false, erreur: "" }, action) {
    switch (action.type) {
        case type.POST_PATIENT_SUCCESS:
        return {...state,patient:[...state.patient,action.payload]}

    case type.POST_PATIENT_FAILURE:
        return {...state,erreur:action.payload,loading:false}
        case type.POST_ORDONNACE_SUCCESS:
        return {...state,ordonnace:[...state.ordonnace,action.payload]}

    case type.POST_ORDONNACE_FAILURE:
        return {...state,erreur:action.payload,loading:false}


    default:
      return state;
  }
}
